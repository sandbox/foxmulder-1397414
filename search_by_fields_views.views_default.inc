<?php
// $Id: $

/**
 * @file
 * Include file for the search_by_fields_views module.
 */

/**
 * Implements hook_views_default_views().
 */
function search_by_fields_views_views_default_views() {
  $views = array();
  $view = new view;
  $view->name = 'search_by_fields';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = t('Search by fields');
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = TRUE;

  // Display: Master 
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options = array(
    'title'        => t('Search by fields'),
    'access'       => array(
      'type'       => 'perm'
    ),
    'cache'        => array(
      'type'       => 'none'
    ),
    'query'        => array(
      'type'       => 'views_query',
      'options'    => array(
        'query_comment' => FALSE
      ),
    ),
    'exposed_form' => array(
      'type'       => 'basic',
      'options'    => array(
        'reset_button_label' => t('Reset')
      )
    ),
    'pager'        => array(
      'type'       => 'full',
      'options'    => array(
        'items_per_page' => '10'
      )
    ),
    'style_plugin' => 'default',
    'row_plugin'   => 'fields',
    'fields'       => array(
      'title'      => array(
        'id'    => 'title', 
        'table' => 'node', 
        'field' => 'title',
        'label' => '',
        'alter' => array(
          'alter_text'    => 0,
          'make_link'     => 0,
          'absolute'      => 0,
          'word_boundary' => 0,
          'ellipsis'      => 0,
          'strip_tags'    => 0,
          'trim'          => 0,
          'html'          => 0,
        ),
        'hide_empty'   => 0,
        'empty_zero'   => 0,
        'link_to_node' => 1, 
      ),
    ),
    'sorts' => array(
      'created' => array(
        'id'    => 'created',
        'table' => 'node',
        'field' => 'created',
        'order' => 'DESC',
      ),
    ),
    'filters' => array(
      'status' => array(
        'id'     => 'status',
        'table'  => 'node',
        'field'  => 'status',
        'value'  => 1,
        'group'  => 0,
        'expose' => array(
          'operator' => FALSE
        )
      )
    ),
  );
  // Display: Page 
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'search-by-fields';
  $views[$view->name] = $view;

  $types = node_type_get_types();
  foreach ($types as $type => $type_data) {
    $clone = $view->clone_view();
    $clone->name = 'search_by_fields_' . $type_data -> type;
    $clone->disabled = FALSE;
    $clone->display['default']->display_options['title'] = t('Search %type by its fields', array('%type' => $type_data -> name));
    $clone->display['default']->display_options['filters']['type'] = array(
      'id'     => 'type',
      'table'  => 'node',
      'field'  => 'type',
      'value'  => array($type_data -> type => $type_data -> type),
      'group'  => 0,
      'expose' => array(
        'operator' => FALSE
      )
    );
    $field_instances = field_info_instances('node', $type_data -> type);
    foreach ($field_instances as $instance => $instance_data) {
      if ($instance != 'body' && $instance != 'field_image') {
        $field_data = field_info_field($instance);
        if (!empty($field_data['storage']['details'])) {
          $base_array = $field_data['storage']['details']['sql']['FIELD_LOAD_CURRENT'];
          $tables = array_keys($base_array);
          $table = $tables[0];
          $foreign_keys = array_keys($base_array[$table]);
          $foreign_key = $foreign_keys[0];
          $filter_key = $base_array[$table][$foreign_key];
          $clone->display['default']->display_options['filters'][$filter_key] = array(
            'id'                => $filter_key,
            'table'             => $table,
            'field'             => $filter_key,
            'exposed'           => TRUE,
            'expose'            => array(
              'operator_id' => $filter_key . '_op',
              'label'       => $instance_data['label'],
              'operator'    => $filter_key . '_op',
              'identifier'  => $filter_key,
              'reduce'      => 0
            ),
            'reduce_duplicates' => 0,
            'type'              => 'select',
            'error_message'     => 1
          );
          if (!empty($field_data['settings']['allowed_values'][0]['vocabulary'])) {
            $clone->display['default']->display_options['filters'][$filter_key]['vocabulary'] = $field_data['settings']['allowed_values'][0]['vocabulary'];
          }
        }
      }
    }
    $clone->display['page']->display_options['path'] = 'search-by-fields/' . $type_data -> type;
    $views[$clone->name] = $clone;
  }
  return $views;
}
