
The Search by fields view module builds a view for all content types and adds the signed fields of this content type to this view as exposed filter.

Installation
------------
Copy search_by_fields_views folder to your module directory and then enable on the admin/modules page.

Usage
-----
After enabling module you will find some new view on admin/structure/views disabled by default. Enable them according to needs.
Go to admin/structure/types/manage/TYPE/fields/FIELDNAME to edit FIELDNAME field.
Near to default value fieldset you can set that this field will be displayed on search-by-fields/TYPE page as an exposed filter or not. The existing fields are enabled by default.

Widget types
------------
By default the Views module allows textfields to the number and text type fields as exposed widget.
The Search by fields views module transforms this widgets into drop-down lists in which the options will be the previosly added values.
Imagine that you have a Person content type with a field named age. If your end-user would like to find persons on search-by-fields/person page by age, she/he doesn't know what options are availabe. If you have some Person type node in which the age has already been added these values will be displayed as options.

This feature can be changed on admin/structure/views/settings/search_by_fields_views page.

Author
------
Novák Attila
96foxmulder69@gmail.com
